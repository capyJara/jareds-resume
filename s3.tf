provider "aws" {}

variable "file_name" {
  type        = string
  default     = "resume.json"
  description = "local file name, and object key"
}

# Create a bucket
resource "aws_s3_bucket" "resume_bucket" {
# Change `bucket` to make it your own
  bucket = "jared-myhrberg-json-resume" 
  force_destroy    = true
}

# Attach Public Read Policy to Bucket
resource "aws_s3_bucket_policy" "allow__public_access" {
  bucket = aws_s3_bucket.resume_bucket.id
  policy = data.aws_iam_policy_document.allow__public_access.json
}

data "aws_iam_policy_document" "allow__public_access" {
  statement {
    effect = "Allow"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
        "s3:GetObject",
        "s3:GetObjectVersion"
    ]
    resources = [
        "arn:aws:s3:::${aws_s3_bucket.resume_bucket.bucket}/*"
    ]
  }
}

# Upload object
resource "aws_s3_object" "resume" {
  bucket = aws_s3_bucket.resume_bucket.id
  key    = var.file_name
  source = var.file_name
  content_disposition = "inline"
  content_type = "application/json"
  content_encoding = "application/json"
  etag = filemd5(var.file_name)
}
