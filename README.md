# What the heck is this??
This repository is for deploying a resume in json format to aws using terraform and gitlab ci. Why tho...... Cause im a nerd :shrug-emoji

Find my resume [here!](https://jared-myhrberg-json-resume.s3.amazonaws.com/resume.json)

### Git
This repo is hosted on [Gitlab](https://gitlab.com/capyJara/jareds-resume/-/tree/main) and uses gitlab ci to deploy any changes to the `main` branch directly to AWS. It will run terraform validate and plan for all new commits regardless of branch.

### AWS
Amazon S3 is hosting the json file. The s3 bucket, access policy and file upload are handled in `s3.tf`.

# How to run/clone it!
### Prerequisits
1. [Amazon account](https://aws.amazon.com/free/?trk=78b916d7-7c94-4cab-98d9-0ce5e648dd5f&sc_channel=ps&sc_campaign=acquisition&sc_medium=ACQ-P|PS-GO|Brand|Desktop|SU|AWS|Core|US|EN|Text&s_kwcid=AL!4422!3!432339156165!e!!g!!aws%20account&ef_id=Cj0KCQjwtvqVBhCVARIsAFUxcRtN3uebu4KX9A-roUZviy_b6AApxc6aBCkky0U3DQ_Q6X6Evpw4RZ4aAmJDEALw_wcB:G:s&s_kwcid=AL!4422!3!432339156165!e!!g!!aws%20account&all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=*all) and [CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) installed. 
- For gitlab ci to work correctly, add these variable to your CI/CD settings in gitlab. It is recommended that you use a dedicated [iam user](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) with `IAMFullAccess` and `AmazonS3FullAccess` access to obtain the access id and key.
   - AWS_ACCESS_KEY_ID
   - AWS_SECRET_ACCESS_KEY
   - AWS_DEFAULT_REGION

2. [Terraform installed](https://learn.hashicorp.com/tutorials/terraform/install-cli).

### Setup
* You'll want to edit the resume.json file to add your own information.
* Within `s3.tf`, edit `aws_s3_bucket` bucket value to your own specs.

1. Run `terraform init`
2. Run `terraform plan` to see staged changes
3. Run `terraform apply` to deploy changes
4. Naviagte to your S3 bucket and object in the amazon dashboard to get your resume's url. Update the README.md resume link.

